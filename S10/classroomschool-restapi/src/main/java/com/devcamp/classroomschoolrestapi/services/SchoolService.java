package com.devcamp.classroomschoolrestapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.classroomschoolrestapi.models.School;

@Service
public class SchoolService {
    @Autowired
    ClassroomService classroomService;

    private School dhfpt = new School(1, "Dai hoc FPT", "Hoa Lac");
    private School hanu = new School(2, "Dai hoc Quoc Gia", "Ha noi");
    private School neu = new School(3, "Dai hoc Kinh Te Quoc Dan", "Duong Lang");

    public ArrayList<School> getAllSchool() {
        ArrayList<School> schools = new ArrayList<>();

        dhfpt.setClassrooms(classroomService.getSoftwareEnginering());
        hanu.setClassrooms(classroomService.getKinhte());
        neu.setClassrooms(classroomService.getVoThuat());

        schools.add(dhfpt);
        schools.add(hanu);
        schools.add(neu);
        return schools;
    }

    public School getSchoolById(int id) {
        ArrayList<School> schools = new ArrayList<>();

        dhfpt.setClassrooms(classroomService.getSoftwareEnginering());
        hanu.setClassrooms(classroomService.getKinhte());
        neu.setClassrooms(classroomService.getVoThuat());

        schools.add(dhfpt);
        schools.add(hanu);
        schools.add(neu);

        for (School school : schools) {
            if (school.getId() == id) {
                return school;
            }
        }
        return null;
    }

    public ArrayList<School> getSchoolByNoStudent(int noStudent) {
        ArrayList<School> schools = getAllSchool();

        ArrayList<School> schoolsWithMoreStudents = new ArrayList<>();

        for (School school : schools) {
            if (school.getTotalStudent() > noStudent) {
                schoolsWithMoreStudents.add(school);
            }
        }

        return schoolsWithMoreStudents;

    }
}
