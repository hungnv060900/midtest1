package com.devcamp.classroomschoolrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassroomschoolRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClassroomschoolRestapiApplication.class, args);
	}

}
