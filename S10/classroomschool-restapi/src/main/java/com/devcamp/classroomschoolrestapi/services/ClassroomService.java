package com.devcamp.classroomschoolrestapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.classroomschoolrestapi.models.Classroom;

@Service
public class ClassroomService {
    private Classroom swt = new Classroom(1, "Software Testing", 25);
    private Classroom swr = new Classroom(2, "Software Requiment", 30);
    private Classroom pro192 = new Classroom(3, "Java", 35);

    private Classroom vov1 = new Classroom(4, "Vovinam 1", 50);
    private Classroom vov2 = new Classroom(5, "Vovinam 2", 60);
    private Classroom vov3 = new Classroom(6, "Vovinam 3", 66);

    private Classroom ibi = new Classroom(7, "Kinh te doi ngoai", 20);
    private Classroom enm = new Classroom(8, "Truyen thong", 21);
    private Classroom sci = new Classroom(9, "Kinh te toan cau", 33);

    public ArrayList<Classroom> getSoftwareEnginering() {
        ArrayList<Classroom> classrooms = new ArrayList<>();
        classrooms.add(swt);
        classrooms.add(swr);
        classrooms.add(pro192);

        return classrooms;
    }
    public ArrayList<Classroom> getKinhte() {
        ArrayList<Classroom> classrooms = new ArrayList<>();
        classrooms.add(ibi);
        classrooms.add(enm);
        classrooms.add(sci);

        return classrooms;
    }
    public ArrayList<Classroom> getVoThuat() {
        ArrayList<Classroom> classrooms = new ArrayList<>();
        classrooms.add(vov1);
        classrooms.add(vov2);
        classrooms.add(vov3);

        return classrooms;
    }
    public ArrayList<Classroom> getAllClassroom() {
        ArrayList<Classroom> classrooms = new ArrayList<>();
        classrooms.add(vov1);
        classrooms.add(vov2);
        classrooms.add(vov3);
        classrooms.add(ibi);
        classrooms.add(enm);
        classrooms.add(sci);
        classrooms.add(swt);
        classrooms.add(swr);
        classrooms.add(pro192);

        return classrooms;
    }

    public ArrayList<Classroom> getClassroomByNoStudent( int noStudent){
        ArrayList<Classroom> classrooms = getAllClassroom();
        ArrayList<Classroom> classroomsWithMoreStudents = new ArrayList<>();
       
        for (Classroom classroom : classrooms) {
            if(classroom.getNoStudent()>noStudent){
                classroomsWithMoreStudents.add(classroom);
            }
        }
        return classroomsWithMoreStudents;
    }

}
