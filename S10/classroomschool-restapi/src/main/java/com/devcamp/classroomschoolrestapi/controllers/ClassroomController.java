package com.devcamp.classroomschoolrestapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.classroomschoolrestapi.models.Classroom;
import com.devcamp.classroomschoolrestapi.services.ClassroomService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ClassroomController {
    @Autowired
    ClassroomService classroomService;

    @GetMapping("/classrooms")
    public ArrayList<Classroom> getAllClass() {
        return classroomService.getAllClassroom();
    }

    @GetMapping("/classrooms/{noStudent}")
    public ArrayList<Classroom> result(@PathVariable int noStudent){
        return classroomService.getClassroomByNoStudent(noStudent);
    }
}
