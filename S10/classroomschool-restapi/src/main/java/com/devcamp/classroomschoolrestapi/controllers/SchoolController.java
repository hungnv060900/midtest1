package com.devcamp.classroomschoolrestapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.classroomschoolrestapi.models.School;
import com.devcamp.classroomschoolrestapi.services.SchoolService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class SchoolController {
    @Autowired
    SchoolService schoolService;

    @GetMapping("/schools")
    public ArrayList<School> getAllSchool() {
        return schoolService.getAllSchool();
    }

    @GetMapping("/schools/{schoolId}")
    public School getSchoolById(@PathVariable int schoolId) {
        return schoolService.getSchoolById(schoolId);
    }

    @GetMapping("/schools/{noNumber}")
    public ArrayList<School> result(@PathVariable int noNumber){
        return schoolService.getSchoolByNoStudent(noNumber);
    }
}
