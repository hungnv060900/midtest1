package com.devcamp.restapi.services;


import org.springframework.stereotype.Service;

@Service
public class S20Service {

    // YC1
    public String reverseString(String input) {
        StringBuilder reversed = new StringBuilder(input).reverse();
        return reversed.toString();
    }

    // YC2
    public String checkPalindrome(String input) {
        String reversed = new StringBuilder(input).reverse().toString();
        return input.equalsIgnoreCase(reversed) ? "Yes, it's a palindrome" : "No, it's not a palindrome";
    }

    // YC3
    public String removeDuplicates(String input) {
        StringBuilder result = new StringBuilder();
        for (char c : input.toCharArray()) {
            if (result.indexOf(String.valueOf(c)) == -1) {
                result.append(c);
            }
        }
        return result.toString();
    }

    // YC4
    public static String concatenateAndEqualizeLength(String str1, String str2) {
        int len1 = str1.length();
        int len2 = str2.length();
        
        // So sánh độ dài của hai chuỗi
        while (len1 > len2) {
            str1 = str1.substring(1); // Cắt bỏ ký tự đầu của str1
            len1--;
        }
        while (len2 > len1) {
            str2 = str2.substring(1); // Cắt bỏ ký tự đầu của str2
            len2--;
        }
        
        // Gắn chuỗi lại với nhau
        return str1 + str2;
    }
}
