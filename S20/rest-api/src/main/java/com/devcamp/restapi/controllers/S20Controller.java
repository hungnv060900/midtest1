package com.devcamp.restapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapi.services.S20Service;

@RestController
@CrossOrigin
@RequestMapping("/")
public class S20Controller {
    @Autowired
    S20Service s20Service;

    @GetMapping("/reverse")
    public String result1(@RequestParam String input) {
        return s20Service.reverseString(input);
    }

    @GetMapping("/check-palindrome")
    public String result2(@RequestParam String input) {
        return s20Service.checkPalindrome(input);
    }

    @GetMapping("/remove-duplicatess")
    public String result3(@RequestParam String input){
        return s20Service.removeDuplicates(input);
    }

    @GetMapping("/concatenate")
    public String result4(@RequestParam String str1, @RequestParam String str2) {
        String result = s20Service.concatenateAndEqualizeLength(str1, str2);
        return result;
    }
}
